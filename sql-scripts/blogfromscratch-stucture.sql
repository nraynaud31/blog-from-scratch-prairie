--
-- Structure de la table `articles`
--
DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles` (
  `id` integer PRIMARY KEY autoincrement,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `image_url` varchar(255) NOT NULL,
  `image_alt` varchar(255) NOT NULL,
  `publised_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reading_time` int(11) NOT NULL,
  `author_id` int(11) NOT NULL
);

--
-- Structure de la table `articles_categories`
--
DROP TABLE IF EXISTS `articles_categories`;
CREATE TABLE `articles_categories` (
  `article_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
);

--
-- Structure de la table `authors`
--
DROP TABLE IF EXISTS `authors`;
CREATE TABLE `authors` (
  `id` integer PRIMARY KEY autoincrement,
  `email` varchar(255) NOT NULL unique,
  `password` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL
);

--
-- Structure de la table `categories`
--
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` integer PRIMARY KEY autoincrement,
  `category` varchar(255) NOT NULL
);


