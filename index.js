const http = require('http'); // Requis pour envoyer du HTTP
const fs = require('fs'); // FileSystem, permet de lire les fichiers (CSS, DB, etc)
const Database = require('better-sqlite3'); // Lib pour les DB

const db = new Database('./db/data.db', {verbose: () => { }});//Verbose = plus d'options

// Headers
const header_value = "text/html; charset=utf-8";
const html_head_home = `<!DOCTYPE html>
<html>
<head>
<title>Blog from scratch - Accueil</title>
<link rel='stylesheet' href='public/stylesheets/style.css'>
</head><body>`;

const html_head_article = `<!DOCTYPE html>
<html>
<head>
<title>Blog from scratch - Articles</title>
<link rel='stylesheet' href='public/stylesheets/style.css'>
</head><body>`;

const html_nav = `
            <header>
            <ul class='head1'>
            <li class='logo'><img src='images/logo.png'></li>
            <li><h1>Blog from scratch</h1></li>
            </ul>
            <ul class='button1'>
            <li id='premier_li'><a href='#'>Filtrer par auteur</a></li>
            <li><a href='#'>Filtrer par catégorie</a></li>
            </ul>
            
            </header>`;

const html_back = `<a class='clickable_return' href='/'> < retour</a>`;


const html_footer = `<p class='copyright'>©2020 - Blog from scratch</p>`;

/* A UTILISER QU'UNE SEULE FOIS !!!!!
const migration = fs.readFileSync('./sql-scripts/blogfromscratch-stucture.sql', 'utf-8');
db.exec(migration);

const migrationData = fs.readFileSync('./sql-scripts/blogfromscratch-data.sql', "utf-8");
db.exec(migrationData);
*/
const server = http.createServer((req, res) => {
    res.setHeader('Content-Type', header_value);
   // Permet d'utiliser le CSS
    if (req.url.indexOf('.css') != -1) {

        fs.readFile(__dirname + '/public/stylesheets/style.css', function (err, data) {
            if (err) console.log(err);
            res.writeHead(200, { 'Content-Type': 'text/css' });

            res.write(data);
            res.end();
        });
        return;
    }

    // Permet de lire le logo
    if(req.url.indexOf('.png') != -1){
        fs.readFile(__dirname + '/images/logo.png', function (err, data){
            if(err) console.log(err);
            res.writeHead(200, {'Content-Type': 'images/png'});

            res.write(data);
            res.end();
        });
        return;
    }
    /*
    const row = db.prepare("SELECT * FROM articles").all();
    console.log(req.url)
    res.end("<h1>"+row[1].title+"</h1>" + "<img src="+row[1].image_url+" alt="+row[1].image_alt+">" + row[1].content)*/
    switch(req.url){

        // Page d'accueil
        case '/':
            const box = db.prepare("SELECT * FROM articles").all();
            console.log(box)
            let result = '';
            box.forEach(article => {
                const content = article.content.split("</p>");
                result += `
                <article class='box'>
                <h2 style='color:red;'>
                ${article.title}</h2>
                <img class="img_accueil" src=${article.image_url} alt= ${article.image_alt}>
                ${content[0]} ${content[1]}
                <a href="http://127.0.0.1:3000/article${article.id}">Lire la suite</a>
                </article>
                `
            });
            res.end(html_head_home + html_nav + result + html_footer);
            break;

            // Pages d'articles
        case '/article0':
            const row0 = db.prepare("SELECT * FROM articles").all();
            console.log(req.url)
            res.end(html_head_article + html_nav + html_back +"<div style='background-color: #FFFFFF; position: relative; margin-bottom: 50px; border: red 3px solid;padding-top: 300px; padding-right: 2.5%; padding-left: 2.5%; margin-top: 50px; margin-left: 9%; margin-right: 9%; width: 70%; height: 90%; overflow: auto; text-align: justify;'>" + "<img style='width: 75%; height: 25%; max-height: 300px; position: absolute; top: 20px; left: 13%;' src="+row0[0].image_url + " alt="+row0[0].image_alt+">" + "<h1 style='color: red;'>"+row0[0].title+"</h1>" + row0[0].content) + "</div>";
            break;

        case '/article1':
            const row1 = db.prepare("SELECT * FROM articles").all();
            console.log(req.url)
            res.end(html_head_article + html_nav + html_back +"<div class= article1>" + "<h1 style='color: red;'>"+row1[1].title+"</h1>" + "<img src="+row1[1].image_url+ " alt="+row1[1].image_alt+">" + row1[1].content) + "</div>";
            break;

        case '/article2':
            const row2 = db.prepare("SELECT * FROM articles").all();
            console.log(req.url)
            res.end(html_head_article + html_nav + html_back + "<div class= article2>" + "<h1 style='color: red;'>"+row2[2].title+"</h1>" + "<img src="+row2[2].image_url+ " alt="+row2[2].image_alt+">" + row2[2].content) + "</div>";
            break;

        case '/article3':
            const row3 = db.prepare("SELECT * FROM articles").all();
            console.log(req.url)
            res.end(html_head_article + html_nav + html_back +"<div class= article3>" + "<h1 style='color: red;'>"+row3[3].title+"</h1>" + "<img src="+row3[3].image_url+ " alt="+row3[3].image_alt+">" + row3[3].content) + "</div>";
            break;

        case '/article4':
            const row4 = db.prepare("SELECT * FROM articles").all();
            console.log(req.url)
            res.end(html_head_article + html_nav + html_back +"<div class= article4>" + "<h1 style='color: red;'>"+row4[4].title+"</h1>" + "<img src="+row4[4].image_url+ " alt="+row4[4].image_alt+">" + row4[4].content) + "</div>";
            break;

        case '/article5':
            const row5 = db.prepare("SELECT * FROM articles").all();
            console.log(req.url)
            res.end(html_head_article + html_nav + html_back +"<div class= article5>" + "<h1 style='color: red;'>"+row5[5].title+"</h1>" + "<img src="+row5[5].image_url+ " alt="+row5[5].image_alt+">" + row5[5].content) + "</div>";
            break;
                    
    }
});

server.listen(3000, '127.0.0.1', () => {
    console.log("Server is running at http://127.0.0.1:3000/");
});