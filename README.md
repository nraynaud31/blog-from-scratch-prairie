### Blog from prairie scratch

[Wireframes](https://simplonline-v3-prod.s3.eu-west-3.amazonaws.com/media/v2/brief/attachments/f1fd088c-87fb-4eba-a956-5826478c3bba.pdf)
[Database](https://simplonline-v3-prod.s3.eu-west-3.amazonaws.com/media/v2/brief/attachments/cf3fa035-3701-4fe2-a5b6-b3c84239f725.zip)


# Contexte du projet
Créer un blog de A à Z, ce n'est sorcier !

[Ceci est la version prairie. La version complète est aussi disponible "Blog from scratch"]

# Développement de l'interface web statique

**Ressource(s) disponible(s)**: - Wireframes de l'application

A partir des wireframes, réaliser les pages HTML statiques du blog avec des données factices (3 billets de blog) .

**Développement de l'interface web dynamique**

Rendre l'interface dynamique en récupérant les billets de blog présent dans la base de données.

La page d'accueil devra lister les billets du plus récent au plus anciens, il sera possible de filtrer cette liste par auteurs et/ou par catégorie. Depuis la page d'un billet de blog, un rebond doit être possible sur l'auteurs et sur les catégories.

**Un peu d'éco-conception**

Temps de réalisation: indéterminé

Réduire au maximum l'empreinte carbone de la plateforme.

**Un peu d'accessibilité**

Temps de réalisation: indéterminé

Corriger les défauts d’accessibilité des développements réalisés (Il ne devrait pas en avoir !).

**Et le tri**

Temps de réalisation: indéterminé

Permettre de trier la liste des articles de blog par auteur et/ou par catégorie

**Modalités pédagogiques**
2j

**Livrables**
Un repo gitlab

*--  
Attention, un projet ne contenant pas de fichier README à la racine ne sera pas corrigé.*
